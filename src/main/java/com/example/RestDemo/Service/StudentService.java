package com.example.RestDemo.Service;

import com.example.RestDemo.Repository.AdressRepository;
import com.example.RestDemo.Repository.StudentRepository;
import com.example.RestDemo.entity.Address;
import com.example.RestDemo.entity.StudentEntity;
import com.example.RestDemo.request.CreateStudentRequest;
import com.example.RestDemo.request.InQueryRequest;
import com.example.RestDemo.request.UpdateStudentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    StudentRepository studentRepository;
   @Autowired
    AdressRepository adressRepository;

@Cacheable(cacheNames = "students")
    public List<StudentEntity> getAllStudents()
    {
        System.out.println("Hello");
       return studentRepository.findAll();
    }
    public StudentEntity createStudent(CreateStudentRequest createStudentRequest)
    {
       StudentEntity st=new StudentEntity(createStudentRequest);
       Address adress=new Address();
       adress.setStreet(createStudentRequest.getStreet());
       adress.setCity(createStudentRequest.getCity());
       adress=adressRepository.save(adress);
     st.setAddress(adress);
      st= studentRepository.save(st);
      return st;

    }
    @CachePut(cacheNames ="students",key="#updateStudentRequest.getId()")
    public StudentEntity updateStudent(UpdateStudentRequest updateStudentRequest)
    {
       StudentEntity student= studentRepository.getById(updateStudentRequest.getId());
       if(updateStudentRequest.getFirstName()!=null && !updateStudentRequest.getFirstName().isEmpty())
       {
           student.setFirstName(updateStudentRequest.getFirstName());
       }
        if(updateStudentRequest.getLastName()!=null && !updateStudentRequest.getLastName().isEmpty())
       {
           student.setLastName(updateStudentRequest.getLastName());
       }
        if(updateStudentRequest.getEmail()!=null && !updateStudentRequest.getEmail().isEmpty())
        {
            student.setEmail(updateStudentRequest.getEmail());
        }
       //studentRepository.save(student);
       return   studentRepository.save(student);

    }
    @CacheEvict(cacheNames = "students",key="#id")
    public String delteStudent(int id )
    {
        studentRepository.deleteById(id);
        return "Student has been deleted successfully";
    }
   //Get By First Name
    public List<StudentEntity> getByFirstName(String firstName)
    {
        return studentRepository.findByFirstName(firstName);
    }
    //Get Student By FirstName and LastName
    public StudentEntity getByFirstAndLastName(String firstName, String lastName)
    {
        // return studentRepository.findByFirstNameAndLastName(firstName,lastName);
        return studentRepository.getStudentByFirstNameAndLastNameJP(firstName,lastName);
    }
    //Get Student By FirstName OR LastName

    public List<StudentEntity> getByFirstORLastName(String firstName, String lastName)
    {
        return studentRepository.findByFirstNameOrLastName(firstName,lastName);
    }
    //In Query
    public List<StudentEntity> getByFirstNamesIn(InQueryRequest inQueryRequest)
    {
        return studentRepository.findByFirstNameIn((inQueryRequest.getFirstName()));
    }
    //Pagination Querry
    public List<StudentEntity> getByPage(int pageNo,int pageSize)
    {
        Pageable pageable= PageRequest.of(pageNo-1,pageSize);
        return studentRepository.findAll(pageable).getContent();
    }
    //Get All with Sorted
public List<StudentEntity>  getAllSorted()
{
    Sort sort=Sort.by(Sort.Direction.ASC,"firstName","lastName");
    return studentRepository.findAll(sort);
}

// Update By JPQL Mapping
    public void updateStudentByFirstNameJPQL(int id ,String firstName)
    {
        studentRepository.updateFirstName(id,firstName);
    }

    //Get Student By City
    public List<StudentEntity> getStudentByCity(String city)
    {
  return studentRepository.findByAddressCity(city);
    }
}

