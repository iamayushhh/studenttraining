package com.example.RestDemo.Response;

import com.example.RestDemo.Controller.Student;
import com.example.RestDemo.entity.StudentEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentResponse {
    @JsonIgnore
    private long id;
    private String firstName;
    private String lastName;
    private String email;

    private String street;
    private String city;


public StudentResponse(StudentEntity student)
{
this.id=student.getId();
this.firstName=student.getFirstName();
this.lastName=student.getLastName();
this.email=student.getEmail();
this.street=student.getAddress().getStreet();
this.city=student.getAddress().getCity();
}

    public StudentResponse(long id, String firstName, String lastName, String email, String street, String city) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.street = street;
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
