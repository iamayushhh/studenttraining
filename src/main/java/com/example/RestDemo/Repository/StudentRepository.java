package com.example.RestDemo.Repository;

import com.example.RestDemo.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Integer> {
    //Find By Perticular Column
List<StudentEntity> findByFirstName(String firstName);
StudentEntity findByFirstNameAndLastName(String firstName,String lastName);
   List<StudentEntity>  findByFirstNameOrLastName(String firstName,String lastName);
   List<StudentEntity> findByFirstNameIn(List<String> firstNames);
   @Query("from StudentEntity where firstName=:firstName and lastName=:lastName")
   StudentEntity getStudentByFirstNameAndLastNameJP(String firstName,String lastName);
   @Modifying
   @Transactional
   @Query("update StudentEntity set firstName=:firstName where id=:id")
    void updateFirstName(int id,String firstName);
   List<StudentEntity> findByAddressCity(String address);
}
