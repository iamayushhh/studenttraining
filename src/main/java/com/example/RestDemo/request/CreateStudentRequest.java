package com.example.RestDemo.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.NotFound;

import javax.validation.constraints.NotBlank;

public class CreateStudentRequest {
    @NotBlank(message="First Name is Required")
    private String firstName;
    private String lastName;
    private String email;
    private String street;
    private String city;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
