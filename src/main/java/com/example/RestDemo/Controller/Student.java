package com.example.RestDemo.Controller;

import com.example.RestDemo.Response.StudentResponse;
import com.example.RestDemo.Service.StudentService;
import com.example.RestDemo.entity.StudentEntity;
import com.example.RestDemo.request.CreateStudentRequest;
import com.example.RestDemo.request.InQueryRequest;
import com.example.RestDemo.request.UpdateStudentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController


public class Student {
    @Autowired
    StudentService studentService;
    @GetMapping("/get")
public List<StudentResponse> getAllStudentsAyush()
{
List<StudentEntity> stud=studentService.getAllStudents();
List<StudentResponse> studentResponsesList=new ArrayList<StudentResponse>();
stud.stream().forEach(studentEntity -> {studentResponsesList.add(new StudentResponse(studentEntity));});
return studentResponsesList;
}
//POST
    @PostMapping("/create")
    public StudentEntity createStudent(@Valid @RequestBody CreateStudentRequest createStudentRequest)
    {
      StudentEntity student=studentService.createStudent(createStudentRequest);
      return student;
     //return studentService.createStudent(createStudentRequest);


    }
    //PUT
    @PutMapping("/update")
    public StudentResponse updateStudent(@Valid @RequestBody UpdateStudentRequest updateStudentRequest)
    {
      StudentEntity studentEntity=studentService.updateStudent(updateStudentRequest);
      return new StudentResponse(studentEntity);
    }
   //Delete - http://localhost:8090/delete?id=11
//    @DeleteMapping("/delete")
//    public String deletedStudent(@RequestParam int id)
//    {
//              return studentService.delteStudent(id);
//    }
   @DeleteMapping("/delete/{id}")
   public String deletedStudent(@PathVariable int id)
   {
       return studentService.delteStudent(id);
   }


   //Get the Student By its name (Get Element by Column
    @GetMapping("/getByFirstName/{firstName}")
    public List<StudentResponse> getStudentByFirstName(@PathVariable String firstName)
    {
      List<StudentEntity> studentList=studentService.getByFirstName(firstName);
      List<StudentResponse> studentResponsesList=new ArrayList<StudentResponse>();
      studentList.stream().forEach(studentEntity -> {studentResponsesList.add(new StudentResponse(studentEntity));});
        return studentResponsesList;

    }
    //Get the Student with AND Querry
    @GetMapping("/getByFirstNameAndLastName/{firstName}/{lastName}")
    public StudentResponse getByFirstNameAndLastName(@PathVariable String firstName,@PathVariable String lastName)
    {
       StudentEntity st=studentService.getByFirstAndLastName(firstName,lastName);
       return new StudentResponse(st);
    }
    //Get the Student with OR querry
    @GetMapping("/getByFirstNameOrLastName/{firstName}/{lastName}")
    public List<StudentResponse> getStudentByFirstNameOrLastName(@PathVariable String firstName)
    {
        List<StudentEntity> studentList=studentService.getByFirstName(firstName);
        List<StudentResponse> studentResponsesList=new ArrayList<StudentResponse>();
        studentList.stream().forEach(studentEntity -> {studentResponsesList.add(new StudentResponse(studentEntity));});
        return studentResponsesList;

    }
    //In Querry
    @GetMapping("/getByFirstNames")
    public List<StudentResponse> getStudentByFirstNameIn(@RequestBody InQueryRequest inQueryRequest)
    {
        List<StudentEntity> studentList=studentService. getByFirstNamesIn(inQueryRequest);

        List<StudentResponse> studentResponsesList=new ArrayList<StudentResponse>();
        studentList.stream().forEach(studentEntity -> {studentResponsesList.add(new StudentResponse(studentEntity));});
        return studentResponsesList;

    }
    // Get Data By Pagination
    @GetMapping("/getAllWithPagination")
    public List<StudentResponse> getAllWithPagination(@RequestParam int pageNo,@RequestParam int pageSize)
    {
        List<StudentEntity> studentList=studentService.getByPage(pageNo,pageSize);

        List<StudentResponse> studentResponsesList=new ArrayList<StudentResponse>();
        studentList.stream().forEach(studentEntity -> {studentResponsesList.add(new StudentResponse(studentEntity));});
        return studentResponsesList;
    }

    //Get All data By Sorting
    @GetMapping("/getAllWithSortedOrder")
    public List<StudentResponse> getAllWithSortedOrder()
    {
        List<StudentEntity> studentList=studentService.getAllSorted();

        List<StudentResponse> studentResponsesList=new ArrayList<StudentResponse>();
        studentList.stream().forEach(studentEntity -> {studentResponsesList.add(new StudentResponse(studentEntity));});
        return studentResponsesList;
    }

    //Put Maping
    @PutMapping("updateFirstName/{id}/{firstName}")
    public String updateFirstNameWithJpql(@PathVariable int id,@PathVariable String firstName)
    {
       studentService.updateStudentByFirstNameJPQL(id,firstName);
       return "Updated Sucess";
    }
    //Get By City
    @GetMapping("/getWithCity/{city}")
    public List<StudentResponse> getStudentBycity(@PathVariable String city)
    {
        List<StudentEntity> studentList=studentService.getStudentByCity(city);

        List<StudentResponse> studentResponsesList=new ArrayList<StudentResponse>();
        studentList.stream().forEach(studentEntity -> {studentResponsesList.add(new StudentResponse(studentEntity));});
        return studentResponsesList;
    }

}
