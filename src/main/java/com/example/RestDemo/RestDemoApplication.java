package com.example.RestDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
//@ComponentScan({"com.example.RestDemo.Controller.Student","com.example.RestDemo.Service.StudentService"})
//@EntityScan("com.example.RestDemo.entity.StudentEntity")
//@EnableJpaRepositories("com.example.RestDemo.Repository.StudentRepository")
@EnableSwagger2
@EnableScheduling
@EnableCaching
public class RestDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestDemoApplication.class, args);
	}

}
