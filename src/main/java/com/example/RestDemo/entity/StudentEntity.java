package com.example.RestDemo.entity;

import com.example.RestDemo.request.CreateStudentRequest;

import javax.persistence.*;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="student_data")
public class StudentEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
private int id=1;
    @Column(name="first_Name")
private String firstName;
    @Column(name="last_Name")
private String lastName;
    @Column(name = "email")
private String email;
    @OneToOne
    @JoinColumn(name="adress_id")
    private Address address;

    public Address getAddress() {
        return address;
    }
public StudentEntity()
{
    
}
    public void setAddress(Address address) {
        this.address = address;
    }

    public StudentEntity(CreateStudentRequest createStudentRequest) {
           this.firstName=createStudentRequest.getFirstName();
           this.lastName=createStudentRequest.getLastName();
           this.email=createStudentRequest.getEmail();
    }

    public StudentEntity(int id, String firstName, String lastName, String email, Address address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
    }

    @Override
    public String toString() {
        return "StudentEntity{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
